# Git command cheatsheet

Prepared for 13.11.2018 training

# Basics of bash

- Everything after # is a comment
- Use the Tab key to autocomplete commands and filenames
- Use up and down arrows to show previous commands

```bash
cd foldername     # Change Directory to ./foldername
.                 # current directory
..                # parent directory
mkdir newfolder   # MaKe DIRectory named newfolder
ls                # LiSt files, ls -l long version
nano filename     # open filename in nano text editor

mv file folder/   # MoVe file to folder
mv file file2     # rename file to file2
cp file folder/   # CoPy file fo folder
rm file           # ReMove file, rm -r for directories
```

# Using git in CLI

## Configuring user
please use the same email as in your gitlab account

```bash
git config --global user.name "Jan Kowalski"
git config --global user.email "jan@example.com"
```
## Starting repositories
```bash
git init   # Creates a repository in current directory
git clone https://gitlab.com/melavio-training/first-meeting.git
# Downloads and initializes a local copy of this repository
```

## Staging and committing
```bash
git add .   # Track all files in current folder (can also select specific)
git reset file   # Unstage file (will not be included in the commit)
git diff file   # See what has been changed in file
git checkout file   # Reset local uncommited changes in file
git status   # See what has been changed and what will be commited
git commit -m"Commit message"   # Create a new commit
```

## Looking at repository state
```bash
git log --graph --oneline --all   # Most convenient, can omit some options
```

## Working with remote
```bash
git fetch   # Check what has been changed on remote
git pull    # Download and apply changes to local repository
git push    # Aplly local commits to the remote repository
```

## Working in parallel
```bash
git branch new_branch   # Create a branch named new_branch
git checkout new_branch   # Start working in new_branch

# While working on `branch_a`:
git merge branch_b   # Merges branch_b into branch_a
git rebase master   # Rebases branch_a onto last commit in master
```